package br.com.talentos.estudantes.ti.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import br.com.talentos.estudantes.ti.model.Estudante;
import br.com.talentos.estudantes.ti.repository.EstudanteRepository;

@RestController
@RequestMapping("/")
public class EstudanteController {
	@Autowired
	EstudanteRepository estudanteRepository;
	@GetMapping("/estudantes/lista")
	public ResponseEntity<List<Estudante>> getAllEstudantes(@RequestParam(required = false) String nome) {
		try {
			List<Estudante> estudantes = new ArrayList<>();
			if (nome == null)
				estudanteRepository.findAll().forEach(estudantes::add);
			else
				estudanteRepository.findByNomeContaining(nome).forEach(estudantes::add);
			if (estudantes.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<>(estudantes, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@GetMapping("/estudantes/{id}")
	public ResponseEntity<Estudante> getEstudanteById(@PathVariable("id") long id) {
		Optional<Estudante> estudanteData = estudanteRepository.findById(id);
		if (estudanteData.isPresent()) {
			return new ResponseEntity<>(estudanteData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
		
	@PostMapping("/estudantes")
	public ResponseEntity<Estudante> createEstudante(@RequestBody Estudante estudante) {
		try {
			Estudante _estudante = estudanteRepository
					.save(new Estudante(estudante.getNome(), estudante.getareaTi(), estudante.getEmail()));
			return new ResponseEntity<>(_estudante, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@PutMapping("/estudantes/{id}")
	public ResponseEntity<Estudante> updateEstudante(@PathVariable("id") long id, @RequestBody Estudante estudante) {
		Optional<Estudante> estudanteData = estudanteRepository.findById(id);
		if (estudanteData.isPresent()) {
			Estudante _estudante = estudanteData.get();
			_estudante.setNome(estudante.getNome());
			_estudante.setareaTi(estudante.getareaTi());
			_estudante.setEmail(estudante.getEmail());
			return new ResponseEntity<>(estudanteRepository.save(_estudante), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	@DeleteMapping("/estudantes/{id}")
	public ResponseEntity<HttpStatus> deleteEstudante(@PathVariable("id") long id) {
		try {
			estudanteRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@DeleteMapping("/estudantes")
	public ResponseEntity<HttpStatus> deleteAllEstudantes() {
		try {
			estudanteRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
