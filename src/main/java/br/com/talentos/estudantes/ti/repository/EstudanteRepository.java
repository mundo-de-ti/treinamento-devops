package br.com.talentos.estudantes.ti.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import br.com.talentos.estudantes.ti.model.Estudante;
import org.springframework.stereotype.Repository;

@Repository
public interface EstudanteRepository extends JpaRepository<Estudante, Long> {
   List<Estudante> findByEmail(String email);
   List<Estudante> findByNomeContaining(String nome);
}