package br.com.talentos.estudantes.ti.configs;

public final class Constants {

    public static final String PERFIL = "ADMIN";

    private Constants() {
    }
}
