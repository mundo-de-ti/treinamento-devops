package br.com.talentos.estudantes.ti.model;

import javax.persistence.*;
@Entity
@Table(name = "estudantes")
public class Estudante {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(name = "nome", nullable = false)
	private String nome;
	@Column(name = "areaTi", nullable = false)
	private String areaTi;
	@Column(name = "email", nullable = false)
	private String email;
	public Estudante() {
	}
	public Estudante(String nome, String areaTi, String email) {
		this.nome = nome;
		this.areaTi = areaTi;
		this.email = email;
	}
	public Long getId() {
		return id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getareaTi() {
		return areaTi;
	}
	public void setareaTi(String areaTi) {
		this.areaTi = areaTi;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Override
	public String toString() {
		return "Estudante [id=" + id + ", nome=" + nome + ", areaTi=" + areaTi + ", email=" + email + "]";
	}
}
